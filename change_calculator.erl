-module(change_calculator).
-include_lib("eunit/include/eunit.hrl").

-export([calculate_change/2]).

calculate_change(0, _) -> 1;
calculate_change(_, []) -> 0;
calculate_change(M, _) when M < 0 -> 0;
calculate_change(M, [C|T]) -> calculate_change(M-C, [C|T]) + calculate_change(M, T).

calculate_chage_test_() ->
    [tdd_tests()
    ,advanced_tests()
    ].

tdd_tests() ->
    [?_assertEqual(0, calculate_change(1, [2]))
    ,?_assertEqual(1, calculate_change(1, [1]))
    ,?_assertEqual(1, calculate_change(2, [2]))
    ,?_assertEqual(1, calculate_change(1, [1, 2]))
    ,?_assertEqual(1, calculate_change(3, [2, 3]))
    ,?_assertEqual(1, calculate_change(2, [1]))
    ].

advanced_tests() ->
    [?_assertEqual(1022, calculate_change(300, [5,10,20,50,100,200,500]))
    ,?_assertEqual(18515, calculate_change(419, [2,5,10,20,50]))
    ].
